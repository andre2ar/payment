# Payment app

## How to use

#### Database (MySQL)
There is an file called updb.sql inside the directory up_db, this file is used to create the app database.
Just run this file and the database will be created.

#### Setup
In index.php file the payment app need some configurations. Is necessary to set up user and and password of database.
This can be done changing the variable $dbOptions.

## Performance optimizations

* To use just one file for the tree steps. Just showing the necessary stuff
* Use CDN for all libraries and if the CDN in unavailable to use a local copy

## Code improvements

* I decided not to use an autoload to this app. This a simple app but in a huge project use a autoload is necessery.
* I made my own router but it works well just for this project
* Use explicit promises instead chain ajax requests

## Code structure

I made up to use MVC pattern approach because is very common in web applications and it works very well.

## Notes

Seems that that requested API is not return the correct headers so to can run the test I use a Google Chrome plug-in (https://chrome.google.com/webstore/detail/allow-control-allow-origi/nlfbmbojpeacfghkpbjhddihlkkiljbi) that add Access-Control-Allow-Origin in all packages. 