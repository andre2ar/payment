<?php

class DB
{
    private $dbHost, $dbUser, $dbPassword, $dbName, $dbConnection;

    public function __construct($dbHost, $dbUser, $dbPassword, $dbName)
    {
        $this->dbHost     = $dbHost;
        $this->dbUser     = $dbUser;
        $this->dbPassword = $dbPassword;
        $this->dbName     = $dbName;

        try
        {
            $param              = array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES UTF8');
            $this->dbConnection = new PDO( 'mysql:host=' . $this->dbHost . '; dbname=' . $this->dbName, $this->dbUser, $this->dbPassword, $param);
        }catch(Exception $e)
        {
            print $e->getMessage();
        }
    }

    public function save_user($user_name, $user_last_name, $user_telephone)
    {
        $sql = "INSERT INTO p_users(user_name, user_last_name, user_telephone)
                values (:user_name, :user_last_name, :user_telephone);";

        $preparedSQL = $this->dbConnection->prepare($sql);

        $preparedSQL->bindParam(':user_name', $user_name);
        $preparedSQL->bindParam(':user_last_name', $user_last_name);
        $preparedSQL->bindParam(':user_telephone', $user_telephone);

        if($preparedSQL->execute())
        {
            return $this->dbConnection->lastInsertId();
        }

        return false;
    }

    public function save_address($user_id, $street, $house_number, $zip, $city)
    {
        $sql = "INSERT INTO p_users_address(user_street, user_house_number, user_zip_code, user_city, id_user)
                values (:user_street, :user_house_number, :user_zip_code, :user_city, :id_user);";

        $preparedSQL = $this->dbConnection->prepare($sql);

        $preparedSQL->bindParam(':id_user', $user_id);
        $preparedSQL->bindParam(':user_street', $street);
        $preparedSQL->bindParam(':user_house_number', $house_number);
        $preparedSQL->bindParam(':user_zip_code', $zip);
        $preparedSQL->bindParam(':user_city', $city);

        if($preparedSQL->execute())
        {
            return $this->dbConnection->lastInsertId();
        }

        return false;
    }

    public function save_payment($user_id, $account_owener, $iban, $payment_id)
    {
        print "$user_id $account_owener $iban $payment_id";
        $sql = "INSERT INTO p_payments(user_id, payment_owner, payment_ibam, payment_id)
                values (:user_id, :payment_owner, :payment_ibam, :payment_id);";

        $preparedSQL = $this->dbConnection->prepare($sql);

        $preparedSQL->bindParam(':user_id', $user_id);
        $preparedSQL->bindParam(':payment_owner', $account_owener);
        $preparedSQL->bindParam(':payment_ibam', $iban);
        $preparedSQL->bindParam(':payment_id', $payment_id);

        if($preparedSQL->execute())
        {
            return $this->dbConnection->lastInsertId();
        }

        return false;
    }
}