-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema payment_db
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `payment_db` ;

-- -----------------------------------------------------
-- Schema payment_db
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `payment_db` DEFAULT CHARACTER SET utf8 ;
USE `payment_db` ;

-- -----------------------------------------------------
-- Table `payment_db`.`p_users`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `payment_db`.`p_users` ;

CREATE TABLE IF NOT EXISTS `payment_db`.`p_users` (
  `id_user` INT NOT NULL AUTO_INCREMENT,
  `user_name` VARCHAR(45) NOT NULL,
  `user_last_name` VARCHAR(100) NOT NULL,
  `user_telephone` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id_user`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `payment_db`.`p_users_address`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `payment_db`.`p_users_address` ;

CREATE TABLE IF NOT EXISTS `payment_db`.`p_users_address` (
  `user_street` VARCHAR(100) NOT NULL,
  `user_house_number` VARCHAR(45) NOT NULL,
  `user_zip_code` VARCHAR(20) NOT NULL,
  `user_city` VARCHAR(45) NOT NULL,
  `id_user` INT NOT NULL,
  PRIMARY KEY (`id_user`),
  CONSTRAINT `fk_payment_user_address_payment_user`
    FOREIGN KEY (`id_user`)
    REFERENCES `payment_db`.`p_users` (`id_user`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `payment_db`.`p_payments`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `payment_db`.`p_payments` ;

CREATE TABLE IF NOT EXISTS `payment_db`.`p_payments` (
  `user_id` INT NOT NULL,
  `payment_owner` VARCHAR(200) NOT NULL,
  `payment_ibam` VARCHAR(20) NOT NULL,
  `payment_id` VARCHAR(300) NOT NULL,
  PRIMARY KEY (`user_id`),
  CONSTRAINT `fk_table1_payment_user1`
    FOREIGN KEY (`user_id`)
    REFERENCES `payment_db`.`p_users` (`id_user`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
