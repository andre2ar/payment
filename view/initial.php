<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Payment</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- BootStrap -->
        <link rel="stylesheet" href="libraries/css/bootstrap.min.css">

        <!-- FontAwsome -->
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/solid.css" integrity="sha384-VGP9aw4WtGH/uPAOseYxZ+Vz/vaTb1ehm1bwx92Fm8dTrE+3boLfF1SpAtB1z7HW" crossorigin="anonymous">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/fontawesome.css" integrity="sha384-1rquJLNOM3ijoueaaeS5m+McXPJCGdr5HcA03/VHXxcp2kX2sUrQDmFc3jR5i/C7" crossorigin="anonymous">

        <!-- My CSS-->
        <link rel="stylesheet" href="libraries/css/main.css">
    </head>

    <body>
    <div class="container">
        <div class="row">
            <div class="col">
                <div class="payment-form">
                    <h3>Payment registration</h3>
                    <hr>
                    <form>
                        <div class="form-group">
                            <?php
                            if(isset($_COOKIE['payment_current_step']) && !empty($_COOKIE['payment_current_step']))
                            {
                                require $_COOKIE['payment_current_step'].".php";
                            }else if(file_exists(__DIR__."/".$view_name.".php"))
                            {
                                require $view_name.".php";
                            }else require 'step1.php';
                            ?>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <script src="libraries/js/jquery-3.3.1.min.js"></script>
    <script src="libraries/js/main.js"></script>
    </body>
</html>
