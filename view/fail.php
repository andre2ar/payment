<h3>Fail</h3>

<p>
    Server isn't returning <i>Access-Control-Allow-Origin</i> header.
</p>

<button class="btn btn-primary" type="button" onclick="window.location = 'step1'">
    Try again
</button>