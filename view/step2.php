<label>Street</label>
<input class="form-control" type="text" id="street" name="street" required>

<label>House number</label>
<input class="form-control" type="number" id="house_number" name="house_number" required>

<label>Zip code</label>
<input class="form-control" type="text" id="zip" name="zip" required>

<label>City</label>
<input class="form-control" type="text" id="city" name="city" required>

<input type="hidden" id="current_step" value="user_address">

<button type="submit" class="btn btn-primary" data-go="step3">
    Next <i class="fas fa-arrow-right"></i>
</button>