<label>First name</label>
<input class="form-control" type="text" id="first_name" name="first_name" required>

<label>Last name</label>
<input class="form-control" type="text" id="last_name" name="last_name" required>

<label>Telephone</label>
<input class="form-control" type="tel" id="telephone" name="telephone" required>

<input type="hidden" id="current_step" value="user_info">
<button type="submit" class="btn btn-primary" data-go="step2">
    Next <i class="fas fa-arrow-right"></i>
</button>