<label>Account owner</label>
<input class="form-control" type="text" id="owner" name="owner" required>

<label>IBAN</label>
<input class="form-control" type="text" id="iban" name="iban" required>

<input type="hidden" id="current_step" value="do_payment">

<button type="submit" class="btn btn-primary" data-go="do_payment">
    Next <i class="fas fa-arrow-right"></i>
</button>