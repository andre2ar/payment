<?php
/**
* Created by André Alvim Ribeiro
 * Email: andre2ar@outlook.com
*/

include_once 'controller/Router.php';
include_once 'libraries/php/functions.php';

date_default_timezone_set('America/Sao_Paulo');

$requested_uri = $_SERVER['REQUEST_URI'];

$dbOptions = [
    'dbHost' => 'localhost',
    'dbUser' => 'root',
    'dbPassword' => 'root',
    'dbName' => 'payment_db'
];

$router = new Router($dbOptions);

$router->route($requested_uri);