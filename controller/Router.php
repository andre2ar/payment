<?php
require './libraries/php/functions.php';
require './model/DB.php';
class Router
{
    public $db;
    public function __construct($dbOptions)
    {
        $this->db = new DB($dbOptions['dbHost'], $dbOptions['dbUser'], $dbOptions['dbPassword'], $dbOptions['dbName']);
    }

    private function clearUrl($url){
        $exUrl = explode("/", $url);
        $finalUrl = [];
        foreach($exUrl as $route){
            if(!empty($route)){
                $finalUrl[] = $route;
            }
        }

        return $finalUrl;
    }

    public function route($url){
        if(isset($_POST['operation']) && !empty($_POST['operation']))
        {
            loadController('Controller', ['dbConnection' => $this->db]);
        }else
        {
            $url = $this->clearUrl($url);

            if(is_array($url)){
                $intendendPlace = end($url);
                loadView($intendendPlace);
            }
        }
    }
}