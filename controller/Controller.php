<?php
define("HOUR", 60*60);
define("DAY", HOUR*24);
define("MONTH", DAY*30);

class OperationsController
{
    public $dbConnection;
    public $router;
    public function __construct($dbConnection)
    {
        $this->dbConnection = $dbConnection;
    }

    public function doOperation($data)
    {
        $operation = $data['operation'];
        switch ($operation){
            case 'save':
                $form_data = json_decode($_POST['form_data'], true);

                $user_id = $this->dbConnection->save_user(
                    $form_data['first_name'],
                    $form_data['last_name'],
                    $form_data['telephone']);

                if($user_id !== false)
                {
                    $this->dbConnection->save_address(
                        $user_id,
                        $form_data['street'],
                        $form_data['house_number'],
                        $form_data['zip'],
                        $form_data['city']);
                }

                return $user_id;
            case 'save_payment':
                $form_data = json_decode($_POST['form_data'], true);

                $this->dbConnection->save_payment(
                    $form_data['customerId'],
                    $form_data['owner'],
                    $form_data['iban'],
                    $form_data['payment_id']
                );
                break;
        }
    }
}

$operationController = new OperationsController($params['dbConnection']);
echo $operationController->doOperation($_POST);