$(function () {
    $("form").submit(function (event) {
        event.preventDefault();
        let goto = $('button').data('go');

        var form_data = JSON.parse(localStorage.getItem('payment_data'));
        if(!form_data)
        {
            form_data = {};
        }

        put_in_object(form_data);

        localStorage.setItem('payment_data', JSON.stringify(form_data));

        if(goto === 'do_payment')
        {
            /*Saving user data*/
            $.ajax({
                method: "POST",
                url: './controller/Controller.php',
                data: {
                    operation: 'save',
                    form_data: JSON.stringify(form_data)
                }
            }).done(function (user_id) {
                if(user_id !== 'false' && user_id !== false)
                {
                    /*Send to external server*/
                    let external_url = "https://37f32cl571.execute-api.eu-central-1.amazonaws.com/default/wunderfleet-recruiting-backend-dev-save-payment-data";
                    let form_data = {};
                    put_in_object(form_data);

                    form_data['customerId'] = user_id;

                    /*Saving payment: EXTERNAL SERVER*/
                    $.ajax({
                        method: "POST",
                        url: external_url,
                        data: JSON.stringify(form_data),
                        crossDomain: true,
                        dataType: 'json',
                        success: function (result) {
                            form_data['payment_id'] = result.paymentDataId;

                            /*Saving payment ID*/
                            $.ajax({
                                method: "POST",
                                url: './controller/Controller.php',
                                data: {
                                    operation: 'save_payment',
                                    form_data: JSON.stringify(form_data)
                                },
                                success: function () {
                                    localStorage.removeItem('payment_data');
                                    delete_cookie('payment_current_step');

                                    setCookie("payment_data_id", result.paymentDataId);
                                    window.location = 'success';
                                }
                            });
                        },
                        error: function (responseData, textStatus, errorThrown) {
                            delete_cookie('payment_current_step');
                            window.location = 'fail';
                        }
                    });
                }
            });
        }else
        {
            setCookie('payment_current_step', goto, 30);
            window.location = goto;
        }
    });
});

function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    var expires = "expires="+ d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function delete_cookie(name) {
    document.cookie = name +'=; Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;';
}

function put_in_object(object) {
    $("input").each(function (index, item) {
        var item_val = $(item).val();
        var item_name = $(item).prop('name');

        if(item_name)
        {
            object[item_name] = item_val;
        }
    });
}