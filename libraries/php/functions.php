<?php
function loadView($view_name, $params = null)
{
    include './view/initial.php';
}

function loadController($controller_name, $params = null)
{
    include './controller/'.$controller_name.'.php';
}